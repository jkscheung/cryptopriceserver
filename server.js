'use strict';

// Constants
const apiInterval = 5000
const apiUrl = "https://api.cryptonator.com/api/ticker"
const currencies = ['btc', 'eth', 'ltc', 'xmr', 'xrp', 'doge', 'dash', 'maid', 'lsk']
const expiryInSeconds = 60
const updateInterval = 60000

// Dependencies
const express = require("express");
const http = require("http");
const socketIo = require("socket.io");
const axios = require("axios");

// Server
const port = process.env.PORT || 3000;
const index = require("./routes/index");
const app = express();
app.use(index);
const server = http.createServer(app);

// Redis client init
const redis = require("redis");
const client = redis.createClient({'host': 'datastore'});
// Redis async functions
const {promisify} = require('util');
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const lPop = promisify(client.lpop).bind(client);
const rPush = promisify(client.rpush).bind(client);

// Socket.io init and set up callbacks for connect/disconnect events
const io = socketIo(server);
let interval;
io.on("connection", socket => {
  console.log("New client connected");
  interval = setInterval(() => updateClient(socket), updateInterval);
  updateClient(socket);
  for (let currency of currencies) {
    const placeholder = {
      ticker: {
        base: currency.toUpperCase(),
        change: "",
        price: "",
        target: "USD",
        volume: ""
      },
      timestamp: ""
    }
    socket.emit("update", placeholder)
  }
  socket.on("disconnect", () => {
    console.log("Client disconnected");
    clearInterval(interval);
  });
});

// Send price updates to connected client
const updateClient = async socket => {
  for (let currency of currencies) {
    // console.log(`${currency}`);
    // If data is in cache, get it from there. Otherwise
    // get it from the API.
    const price = await getAsync(`${currency}-usd`);
    if (price) {
      console.log(`Got ${currency} from cache`);
      socket.emit("update", JSON.parse(price));
    } else {
      // Queue this currency to get price from API
      rPush("queue", currency);
    }
  }
}

// Get currency data from API. Ensure we only have one loop
// accessing the API since there can be multiple copies of this
// server running when scaling.
const getFromApi = async () => {
  const apiLoop = await getAsync("apiLoop")
  if (apiLoop) {
    return
  }
  setAsync("apiLoop", true)
  setInterval(async () => {
    try {
      // Check queue to see if we need to get data from API
      const currency = await lPop("queue")
      if (!currency) {
        return
      }
      const res = await axios.get(`${apiUrl}/${currency}-usd`);
      if (res.status == 200 && res.data.success) {
        console.log(`Got ${currency} from API`);
        // console.log(res.data)
        // Cache data with an expiry time
        setAsync(`${currency}-usd`, JSON.stringify(res.data), 'EX', expiryInSeconds);
      } else {
        console.error(`API call failed for ${currency}`);
        console.error(res.status)
        console.error(res.data)
      }
    } catch (error) {
      console.error(`${error}`)
      console.error(`Error: ${error.code}`);
    }
  }, apiInterval);
};

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Start loop to get data from API
getFromApi()

// Start listening for connections
server.listen(port, () => console.log(`Listening on port ${port}`));
